var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
    index: {
        type: Number
    },
    text: {
        type: String
    },
    completed: {
        type: Boolean
    }
});

const Todo = mongoose.model('Todo', TodoSchema);

function setUpConnection() {
    mongoose.connect('mongodb://pashka:kus_kus_123@ds121494.mlab.com:21494/todos');
}

function listTodos() {
    return Todo.find().sort(`index`);
}

function removeTodo(id) {
    var promise = new Promise(function (resolve, reject) {
        Todo.findById(id, function (err, todo) {
            if (err) {
                reject(err);
            } else {
                Todo.collection.updateMany({
                    index: {
                        $gte: todo.index
                    }
                }, {
                    $inc: {
                        "index": -1
                    }
                });

                resolve(Todo.findById(id).remove());
            }
        });
    });

    return promise;
}

function createTodo(data) {

    const todo = new Todo({
        index: data.index,
        text: data.text,
        completed: data.completed,
    });

    return todo.save();
}

function toggledTodo(data) {
    return Todo.findByIdAndUpdate(data.id, {
        $set: {
            completed: data.completed
        }
    }, function (err, todo) {
        console.log("updated todo" + todo);
    });
}

function editTodo(data) {
    return Todo.findByIdAndUpdate(data.id, {
        $set: {
            text: data.text
        }
    }, function (err, todo) {
        console.log("updated todo" + todo);
    });
}

function moveTodo(data) {
    var promise = new Promise(function (resolve, reject) {
        Todo.findById(data.id, function (err, todo) {
            if (err) {
                reject(err);
            } else {
                const newIndex = data.index;
                const oldIndex = todo.index;

                if (newIndex > oldIndex) {
                    Todo.collection.updateMany({
                        index: {
                            $gte: oldIndex,
                            $lte: newIndex
                        }
                    }, {
                        $inc: {
                            "index": -1
                        }
                    });
                }

                if (newIndex < oldIndex) {
                    Todo.collection.updateMany({
                        index: {
                            $gte: newIndex,
                            $lte: oldIndex
                        }
                    }, {
                        $inc: {
                            "index": 1
                        }
                    });
                }

                resolve(Todo.findByIdAndUpdate(data.id, {
                    $set: {
                        index: newIndex
                    }
                }));
            }
        });
    });

    return promise;
}

module.exports = {
    setUpConnection,
    listTodos,
    createTodo,
    toggledTodo,
    moveTodo,
    removeTodo,
    editTodo,
    Todo
};