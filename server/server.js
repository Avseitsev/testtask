const bodyParser = require('body-parser');
const http = require('http');
const socketIO = require('socket.io');
const db = require('./utils/DataBaseUtils');
db.setUpConnection();

const express = require('express');
const app = express();
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.get('/todos', (req, res, next) => {
  db.listTodos().then(data => res.send(data));
});

app.post('/todos', (req, res, next) => {
  db.Todo.collection.count(function (err, count) {
    const data = {
      index: count,
      text: req.body.text,
      completed: req.body.completed
    }

    db.createTodo(data).then(data => res.send(data));
  });
});

app.put('/todos/:todoId', (req, res, next) => {
  const data = {
    id: req.params.todoId,
    completed: req.body.completed
  }

  db.toggledTodo(data).then(data => res.send(data));
});

app.put('/todo/:todoId', (req, res, next) => {
  const data = {
    id: req.params.todoId,
    text: req.body.text
  }

  db.editTodo(data).then(data => res.send(data));
});

app.put('/moveTodo/:todoId', (req, res, next) => {
  const data = {
    id: req.params.todoId,
    index: req.body.index
  }

  db.moveTodo(data).then(data => res.send(data));
});

app.delete('/todos/:todoId', (req, res, next) => {
  db.removeTodo(req.params.todoId).then(data => res.send(data));
});

//////////////////////////////////////////////////

const port = 8085;
const server = http.createServer(app);
const io = socketIO(server);

io.on('connection', socket => {
  console.log('New client connected')

  socket.on('add todo', (id, text) => {
    console.log('New todo: ', text)
    socket.broadcast.emit('add todo', id, text)
  })

  socket.on('toggle todo', (id, completed) => {
    console.log('Toggle todo: ', id)
    socket.broadcast.emit('toggle todo', id, completed)
  })

  socket.on('move todo', (id, index) => {
    console.log('Move todo: ', id)
    socket.broadcast.emit('move todo', id, index)
  })

  socket.on('remove todo', (id) => {
    console.log('Remove todo: ', id)
    socket.broadcast.emit('remove todo', id)
  })

  socket.on('edit todo', (id, text) => {
    console.log('Edit todo: ', id)
    socket.broadcast.emit('edit todo', id, text)
  })

  socket.on('disconnect', () => {
    console.log('user disconnected')
  })
})

server.listen(port, () => console.log(`Listening on port ${port}`));