// @flow
import {
  ADD_TODO,
  TODO_ADDED,
  TOGGLE_TODO,
  TOGGLED_TODO,
  GET_TODOS,
  RECEIVE_TODOS,
  MOVE_TODO,
  MOVED_TODO,
  REMOVE_TODO,
  REMOVED_TODO,
  EDIT_TODO,
  EDITED_TODO
} from "./actions/actions";

export type Todo = {
  id: string,
  text: string,
  completed: boolean
}

interface TodosState {
  todosData: Todo[],
  isLoading: boolean
}

export interface AddTodoAction { type: typeof ADD_TODO }
export interface TodoAddedAction { type: typeof TODO_ADDED, id: string, text: string }
export interface GetTodosAction { type: typeof GET_TODOS }
export interface ReceiveTodosAction { type: typeof RECEIVE_TODOS, todos: Todo[] }
export interface ToggleTodoAction { type: typeof TOGGLE_TODO, id: string, completed: boolean }
export interface ToggledTodoAction { type: typeof TOGGLED_TODO, id: string, completed: boolean }
export interface MoveTodoAction { type: typeof MOVE_TODO, id: string, index: number }
export interface MovedTodoAction { type: typeof MOVED_TODO, id: string, index: number }
export interface RemoveTodoAction { type: typeof REMOVE_TODO, id: string }
export interface RemovedTodoAction { type: typeof REMOVED_TODO, id: string }
export interface EditTodoAction { type: typeof EDIT_TODO, id: string, text: string }
export interface EditedTodoAction { type: typeof EDITED_TODO, id: string, text: string }

export type TodosAction = AddTodoAction
  | TodoAddedAction
  | GetTodosAction
  | ReceiveTodosAction
  | ToggleTodoAction
  | ToggledTodoAction
  | MoveTodoAction
  | MovedTodoAction
  | RemoveTodoAction
  | RemovedTodoAction;

// eslint-disable-next-line
Array.prototype.move = function(from, to) {
  const elem = this.splice(from, 1)[0];
  this.splice(to, 0, elem);
  return this;
};

const moveTodo = (todoData: Todo[], id: string, index: number): Todo[] => {
  const fromIndex = todoData.findIndex(x => x.id === id);
  
  return todoData.move(fromIndex, index);
};

const removeTodo = (todoData: Todo[], id: string): Todo[] => {
  const index = todoData.findIndex(x => x.id === id);

  todoData.splice(index, 1);
  return todoData;
};

const todos = (state: TodosState, action: TodosAction): TodosState => {
  switch (action.type) {
    case ADD_TODO:
      return state;
    case TODO_ADDED:
      return {
        ...state, todosData: [...state.todosData,
        {
          id: action.id,
          text: action.text,
          completed: false
        }]
      };
    case GET_TODOS:
      return { ...state, isLoading: true };
    case RECEIVE_TODOS:
      return { ...state, todosData: action.todos, isLoading: false };
    case TOGGLE_TODO:
      return {
        ...state, todosData: state.todosData.map(
          todo =>
            todo.id === action.id ? { ...todo, completed: action.completed } : todo
        )
      };
    case TOGGLED_TODO:
      return {
        ...state, todosData: state.todosData.map(
          todo =>
            todo.id === action.id ? { ...todo, completed: action.completed } : todo
        )
      };
    case MOVE_TODO:
      return {...state, todosData: moveTodo(state.todosData, action.id, action.index)};
    case MOVED_TODO:
      return {...state, todosData: moveTodo(state.todosData, action.id, action.index)};
    case REMOVE_TODO:
      return {...state, todosData: removeTodo(state.todosData, action.id) };
    case REMOVED_TODO:
      return {...state, todosData: removeTodo(state.todosData, action.id) };
    case EDIT_TODO: 
      return {
        ...state, todosData: state.todosData.map(
          todo =>
            todo.id === action.id ? { ...todo, text: action.text } : todo
        )
      };
    case EDITED_TODO: 
      return {
        ...state, todosData: state.todosData.map(
          todo =>
            todo.id === action.id ? { ...todo, text: action.text } : todo
        )
      };
    default:
      return state || {
        todosData: [],
        isLoading: false
      };
  }
};

export default todos;
