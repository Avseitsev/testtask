// @flow

export const getTodoList = () => [{
    id: "1",
    text: "1",
    completed: false
  },
  {
    id: "2",
    text: "2",
    completed: true
  },
  {
    id: "3",
    text: "3",
    completed: false
  }
];

const togledTodoList = [{
    id: "1",
    text: "1",
    completed: true
  },
  {
    id: "2",
    text: "2",
    completed: false
  },
  {
    id: "3",
    text: "3",
    completed: false
  }
];

const movedTodoList = [{
    id: "2",
    text: "2",
    completed: true
  },
  {
    id: "3",
    text: "3",
    completed: false
  },
  {
    id: "1",
    text: "1",
    completed: false
  }
];

const removedTodoList = [{
    id: "1",
    text: "1",
    completed: false
  },
  {
    id: "3",
    text: "3",
    completed: false
  }
];

const addedTodoList = [{
    id: "1",
    text: "1",
    completed: false
  },
  {
    id: "2",
    text: "2",
    completed: true
  },
  {
    id: "3",
    text: "3",
    completed: false
  },
  {
    id: "4",
    text: "4",
    completed: false
  }
];

const editedTodoList = [{
    id: "1",
    text: "1",
    completed: false
  },
  {
    id: "2",
    text: "test",
    completed: true
  },
  {
    id: "3",
    text: "3",
    completed: false
  }
];

export const ExpectedTodoLists = {
  togledTodoList,
  movedTodoList,
  removedTodoList,
  addedTodoList,
  editedTodoList
};