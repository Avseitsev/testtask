// @flow
import todos from "../todos";
import {
  ADD_TODO,
  TODO_ADDED,
  TOGGLE_TODO,
  TOGGLED_TODO,
  GET_TODOS,
  RECEIVE_TODOS,
  MOVE_TODO,
  MOVED_TODO,
  REMOVE_TODO,
  REMOVED_TODO,
  EDIT_TODO,
  EDITED_TODO
} from "../actions/actions";
import {
  ExpectedTodoLists,
  getTodoList
} from "./testData";

describe("Test todosReducers", () => {

  it("Reducer for ADD_TODO", () => {
    let state = {};

    state = todos(state, {
      type: ADD_TODO
    });

    expect(state).toEqual({});
  });

  it("Reducer for TODO_ADDED", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: TODO_ADDED,
      id: "4",
      text: "4"
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.addedTodoList
    });
  });

  it("Reducer for GET_TODOS", () => {
    let state = {
      isLoading: false
    };

    state = todos(state, {
      type: GET_TODOS
    });

    expect(state).toEqual({
      isLoading: true
    });
  });

  it("Reducer for RECEIVE_TODOS", () => {
    let state = {
      todosData: [],
      isLoading: true
    };

    state = todos(state, {
      type: RECEIVE_TODOS,
      todos: getTodoList()
    });

    expect(state).toEqual({
      todosData: getTodoList(),
      isLoading: false
    });
  });

  it("Reducer for TOGGLE_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: TOGGLE_TODO,
      id: "1",
      completed: true
    });

    state = todos(state, {
      type: TOGGLE_TODO,
      id: "2",
      completed: false
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.togledTodoList
    });
  });

  it("Reducer for TOGGLED_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: TOGGLED_TODO,
      id: "1",
      completed: true
    });

    state = todos(state, {
      type: TOGGLE_TODO,
      id: "2",
      completed: false
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.togledTodoList
    });
  });

  it("Reducer for MOVE_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: MOVE_TODO,
      id: "1",
      index: "2",
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.movedTodoList,
    });
  });

  it("Reducer for MOVED_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: MOVED_TODO,
      id: "1",
      index: "2",
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.movedTodoList,
    });
  });

  it("Reducer for REMOVE_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: REMOVE_TODO,
      id: "2",
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.removedTodoList,
    });
  });

  it("Reducer for REMOVED_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: REMOVED_TODO,
      id: "2",
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.removedTodoList,
    });
  });

  it("Reducer for EDIT_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: EDIT_TODO,
      id: "2",
      text: "test"
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.editedTodoList,
    });
  });

  it("Reducer for EDITED_TODO", () => {
    let state = {
      todosData: getTodoList()
    };

    state = todos(state, {
      type: EDITED_TODO,
      id: "2",
      text: "test"
    });

    expect(state).toEqual({
      todosData: ExpectedTodoLists.editedTodoList,
    });
  });
});