// @flow
import React, { Component } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import Todo from "./todo/Todo";
import { TodoListWrapper } from "./TodoList.style";

export type TodoObject = {
  id: string,
  completed: boolean,
  text: string
}

type Props = {
  todos: TodoObject[],
  toggleTodo: Function,
  moveTodo: Function,
  removeTodo: Function,
  editTodo: Function,
};

type State = {
  todos: TodoObject[]
};

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export default class TodoList extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      todos: props.todos
    };
  };

  componentWillReceiveProps(nextProps: Props) {
    this.setState({
      todos: nextProps.todos
    });
  }

  onClick = (id: string): void => {
    const { toggleTodo } = this.props;

    toggleTodo(id);
  }

  onSave = (id: string, text: string): void => {
    const { editTodo } = this.props;
    
    editTodo(id, text);
  }

  onDragEnd = (result: Object) => {
    const { moveTodo, removeTodo } = this.props;
    const { todos } = this.state;
    const { draggableId, destination, source } = result;

    // dropped outside the list
    if (!destination) {
      removeTodo(draggableId);
      return;
    }

    this.setState({
      todos: reorder(
        todos,
        source.index,
        destination.index
      )
    });

    moveTodo(draggableId, destination.index);
  }

  render() {
    const { todos } = this.state;

    return (
      <TodoListWrapper>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId="droppable">
            {(droppableProvided) => (
              <ul ref={droppableProvided.innerRef}>
                {todos.map((todo, index) => (
                  <Draggable key={todo.id} draggableId={todo.id} index={index}>
                    {(draggableProvided) => (
                      <li ref={draggableProvided.innerRef}
                        {...draggableProvided.draggableProps}
                        {...draggableProvided.dragHandleProps}
                      >
                        <Todo
                          key={todo.id}
                          todo={todo}
                          onClick={this.onClick}
                          onSave={this.onSave}
                       />
                      </li>
                    )}
                  </Draggable>
                ))}
              </ul>
           )}
          </Droppable>
        </DragDropContext>
      </TodoListWrapper>
    );
  }
}
