// @flow
import React from "react";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { ThemeProvider } from "styled-components";
import theme from "../../theme";
import TodoList from "./TodoList";
import "jest-styled-components";

Enzyme.configure({ adapter: new Adapter() });

describe("TodoList component:", () => {

  it("TodoList renders correctly", () => {
    const btn = Enzyme.shallow(
      <ThemeProvider theme={theme} >
        <TodoList moveTodo={jest.fn()} toggleTodo={jest.fn()} todos={
          [{
            id: "testId",
            completed: false,
            text: "text"
          },
          {
            id: "testId",
            completed: false,
            text: "text"
          }]
        } />
      </ThemeProvider>
    );

    expect(btn).toMatchSnapshot();
  });
});
