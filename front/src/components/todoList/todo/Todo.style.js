import styled from "styled-components";

export const TodoWrapper = styled.div`
  display: flex;
  align-items: baseline;
  padding-top: ${p => p.theme.todo.padding}px;

  svg {
    cursor: pointer;
    padding-right: ${p => p.theme.todo.padding}px;
  }
`;

export const TexWrapper = styled.span`
  text-decoration: ${p => p.completed ? "line-through" : "none"};
`;
