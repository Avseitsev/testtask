// @flow
import React from "react";
import renderer from "react-test-renderer";
import { ThemeProvider } from "styled-components";
import theme from "../../../theme";
import Todo from "./Todo";
import "jest-styled-components";

describe("Todo component:", () => {
  it("Completed Todo renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Todo
          onClick={jest.fn()}
          todo={{
            id: "testId",
            completed: true,
            text: "text"
          }} />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Unfinished Todo renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Todo
          onClick={jest.fn()}
          todo={{
            id: "testId",
            completed: false,
            text: "text"
          }} />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
