import styled from "styled-components";

export const FooterWrapper = styled.div`
  align-items: center;
  display: flex;

  button {
    margin-left: ${p => p.theme.footer.button.margin}px;
  }
`;