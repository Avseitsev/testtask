// @flow
import React from "react";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { ThemeProvider } from "styled-components";
import { IntlProvider } from "react-intl";
import messagesEN from "../../translations/en.json";
import theme from "../../theme";
import Footer from "./Footer";
import "jest-styled-components";

Enzyme.configure({ adapter: new Adapter() });

describe("Footer component:", () => {
  const locale = "en";

  it("Footer renders correctly", () => {

    const btn = Enzyme.shallow(
      <IntlProvider locale={locale} messages={messagesEN}>
        <ThemeProvider theme={theme} >
          <Footer />
        </ThemeProvider>
      </IntlProvider>
    );
    
    expect(btn).toMatchSnapshot();
  });
});
