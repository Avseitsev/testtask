// @flow
import React, { PureComponent } from "react";
import { injectIntl, FormattedMessage } from "react-intl";
import FilterLink from "../../containers/FilterLink";
import { VisibilityFilters } from "../../reducers/actions";
import { FooterWrapper } from "./Footer.style";

type Props = {
  intl: Object
};

class Footer extends PureComponent<Props> {
  render() {
    const { intl } = this.props;

    return (
      <FooterWrapper className="footer">
        <FormattedMessage id="footer.title.filters" />
        <FilterLink filter={VisibilityFilters.SHOW_ALL} label={intl.formatMessage({ id: "filter.label.all" })} />
        <FilterLink filter={VisibilityFilters.SHOW_ACTIVE} label={intl.formatMessage({ id: "filter.label.active" })} />
        <FilterLink filter={VisibilityFilters.SHOW_COMPLETED} label={intl.formatMessage({ id: "filter.label.completed" })} />
      </FooterWrapper>
    );
  }
}

export default injectIntl(Footer);
