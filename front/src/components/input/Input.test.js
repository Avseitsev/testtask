// @flow
import React from "react";
import renderer from "react-test-renderer";
import { ThemeProvider } from "styled-components";
import theme from "../../theme";
import Input from "./Input";
import "jest-styled-components";

describe("Input component:", () => {

  it("Input renders correctly", () => {

    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Input onChange={jest.fn()} value="value" placeholder="placeholder" />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
