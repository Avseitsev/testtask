import styled from "styled-components";

export const ButtonWrapper = styled.div`
  button {
    height: ${p => p.theme.button.height}px;
    min-width: ${p => p.theme.button.minWidth}px;
    align-items: center;
    background: ${p => p.disabled ? p.theme.button.bacground.disabledColor : p.theme.button.bacground.activeColor};
    border-radius: ${p => p.theme.button.border.radius}px;
    cursor: ${p => p.disabled ? "unset" : "pointer"};
    color: ${p => p.theme.button.color};
    font-size: 1rem;
    outline: none;

    &:active {
      border: 1px solid ${p => p.theme.button.border.activeColor};
      box-shadow: 0 0 4px 2px  ${p => p.theme.button.boxShadow.color} inset;
      opacity:.95;
    }

    &:hover {
      opacity:.85;
    }
  }
`;
