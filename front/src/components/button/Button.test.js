// @flow
import React from "react";
import renderer from "react-test-renderer";
import { ThemeProvider } from "styled-components";
import theme from "../../theme";
import Button from "./Button";
import "jest-styled-components";

describe("Button component:", () => {
  const label = "label";

  it("Button renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Button onClick={jest.fn()} label={label} />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Desabled Button renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Button onClick={jest.fn()} label={label} disabled />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });


});
