// @flow
import { connect } from "react-redux";
import { toggleTodo, VisibilityFilters, moveTodo, removeTodo, editTodo } from "../reducers/actions";
import TodoList from "../components/todoList/TodoList";

const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case VisibilityFilters.SHOW_ALL:
      return [...todos];
    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(t => t.completed);
    case VisibilityFilters.SHOW_ACTIVE:
      return todos.filter(t => !t.completed);
    default:
      throw new Error(`Unknown filter: ${filter}`);
  }
};

const mapStateToProps = state => ({
  todos: getVisibleTodos(state.todos.todosData, state.filters.visibilityFilter)
});

const mapDispatchToProps = dispatch => ({
  toggleTodo: (id: string) => dispatch(toggleTodo(id)),
  moveTodo: (id: string, index: number) => dispatch(moveTodo(id, index)),
  removeTodo: (id: string) => dispatch(removeTodo(id)),
  editTodo: (id: string, text: string) => dispatch(editTodo(id, text))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
