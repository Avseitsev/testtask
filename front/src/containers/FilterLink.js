// @flow
import { connect } from "react-redux";
import { setVisibilityFilter } from "../reducers/actions";
import Button from "../components/button/Button";

type Props = {
  filter: string,
  label: string
}

const mapStateToProps = (state, ownProps: Props) => ({
  disabled: ownProps.filter === state.filters.visibilityFilter
});

const mapDispatchToProps = (dispatch, ownProps: Props) => ({
  onClick: () => dispatch(setVisibilityFilter(ownProps.filter))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
