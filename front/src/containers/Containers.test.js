// @flow
import React from "react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { ThemeProvider } from "styled-components";
import theme from "../theme";
import AddTodo from "./AddTodo";
import FilterLink from "./FilterLink";
import VisibleTodoList from "./VisibleTodoList";
import Button from "../components/button/Button"; 
import TodoList from "../components/todoList/TodoList";
import { VisibilityFilters } from "../reducers/actions";

Enzyme.configure({ adapter: new Adapter() });

describe("AddTodo container tests",() => {
	const initialState = {  };
	const mockStore = configureStore();
	let store; let wrapper;

	beforeEach(()=>{
			store = mockStore(initialState);
			wrapper = Enzyme.shallow( <Provider store={store}><AddTodo /></Provider> );
	});

	it("Render the AddTodo(SMART)", () => {
		 expect(wrapper.find(AddTodo).length).toEqual(1);
	});
});

describe("FilterLink container tests",() => {
	const initialState = { filters:{ visibilityFilter: VisibilityFilters.SHOW_ALL } };
	const mockStore = configureStore();
	let store; let wrapper; let disabledWrapper;

	beforeEach(() => {
		store = mockStore(initialState);
		wrapper = Enzyme.mount( 
			<Provider store={store}>
				<ThemeProvider theme={theme} >
					<FilterLink filter={VisibilityFilters.SHOW_ALL}/>
				</ThemeProvider>
			</Provider> 
		);
		disabledWrapper = Enzyme.mount( 
			<Provider store={store}>
				<ThemeProvider theme={theme} >
					<FilterLink filter="TEST"/>
				</ThemeProvider>
			</Provider> 
		);
	});

	it("Render the FilterLink(SMART)", () => {
		 expect(wrapper.find(FilterLink).length).toEqual(1);
	});

	it("Check disabled Prop by matching filters ", () => {
		expect(wrapper.find(Button).prop("disabled")).toEqual(true);
		expect(disabledWrapper.find(Button).prop("disabled")).toEqual(false);
 	});
});

describe("VisibleTodoList container tests",() => {
	const todos = { todosData: [{id: "1", completed: true}, {id: "2", completed: false}] };
	const mockStore = configureStore();
	let store; let wrapper;

	it("Render the VisibleTodoList(SMART)", () => {
		const initialState = { 
			filters: { visibilityFilter: VisibilityFilters.SHOW_ALL },
			todos
		};

		store = mockStore(initialState);
		wrapper = Enzyme.mount( 
			<Provider store={store}>
				<ThemeProvider theme={theme} >
					<VisibleTodoList />
				</ThemeProvider>
			</Provider> 
		);

		 expect(wrapper.find(VisibleTodoList).length).toEqual(1);
	});

	it("Check todos Prop by \"ALL\" filter", () => {
		const initialState = { 
			filters: { visibilityFilter: VisibilityFilters.SHOW_ALL },
			todos
		};

		store = mockStore(initialState);
		wrapper = Enzyme.mount( 
			<Provider store={store}>
				<ThemeProvider theme={theme} >
					<VisibleTodoList />
				</ThemeProvider>
			</Provider> 
		);

		expect(wrapper.find(TodoList).prop("todos")).toEqual(todos.todosData);
	 });
	 
	 it("Check todos Prop by \"COMPLETED\" filter", () => {
		const initialState = { 
			filters: { visibilityFilter: VisibilityFilters.SHOW_COMPLETED },
			todos
		};

		store = mockStore(initialState);
		wrapper = Enzyme.mount( 
			<Provider store={store}>
				<ThemeProvider theme={theme} >
					<VisibleTodoList />
				</ThemeProvider>
			</Provider> 
		);

		expect(wrapper.find(TodoList).prop("todos")).toEqual(todos.todosData.filter(todo => todo.completed));
	 });
	 
	 it("Check todos Prop by \"ACTIVE\" filter", () => {
		const initialState = { 
			filters: { visibilityFilter: VisibilityFilters.SHOW_ACTIVE },
			todos
		};

		store = mockStore(initialState);
		wrapper = Enzyme.mount( 
			<Provider store={store}>
				<ThemeProvider theme={theme} >
					<VisibleTodoList />
				</ThemeProvider>
			</Provider> 
		);

		expect(wrapper.find(TodoList).prop("todos")).toEqual(todos.todosData.filter(todo => !todo.completed));
 	});
});
