import styled from "styled-components";

export const PageWrapper = styled.div`
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  overflow-y: hidden;
  position: absolute;
  font-family: 'Nunito Sans', sans-serif;
  font-size: 1.5rem;
  width: 100%;
  min-height: 100%;
  height: auto;
  background-color: ${p => p.theme.page.bacground.color};
`;

export const Header = styled.header`
  position: relative;
  height: ${p => p.theme.page.header.height}px;
  overflow: hidden;
  background: ${p => p.theme.page.header.bacground.color};
  text-transform: uppercase;
  background-image: linear-gradient(141deg,#9fb8ad 1%,#1fc8db 11%,#2cb5e8 75%);
  box-shadow: rgb(72, 75, 77) 0px 2px 15px 1px;

  span {
    transform: translate(50%,-50%);
    position: absolute;
    right: 50%;
    top: 50%;
    font-weight: 600;
  }
`;

export const FormWrapper = styled.div`
  text-align: initial;
  display: inline-table;
  min-height: ${p => p.theme.page.formWrapper.minHeight}px;
  min-width: ${p => p.theme.page.formWrapper.width}px;
  padding: ${p => p.theme.page.formWrapper.padding}px;
  background-color: ${p => p.theme.page.formWrapper.bacground.color};
  background-image: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
  margin-top: ${p => p.theme.page.formWrapper.marginTop}px;
  box-shadow: rgb(72, 75, 77) 0px 5px 10px 1px;

  .footer {
    padding-bottom: ${p => p.theme.page.footer.padding.bottom}px;
  }
`;