// @flow
import HttpWrapper from "./wrapper";

export const getTodos = () => {
  return HttpWrapper.get({
    url: "todos"
  });
};

export const addTodo = (text: string, completed: boolean) => {
  return HttpWrapper.post({
    url: "todos",
    data: {
      text,
      completed
    }
  });
};

export const toggleTodo = (id: string, completed: boolean) => {
  return HttpWrapper.put({
    url: `todos/${id}`,
    data: {
      completed
    }
  });
};

export const editTodo = (id: string, text: string) => {
  return HttpWrapper.put({
    url: `todo/${id}`,
    data: {
      text
    }
  });
};

export const moveTodo = (id: string, index: number) => {
  return HttpWrapper.put({
    url: `moveTodo/${id}`,
    data: {
      index
    }
  });
};

export const removeTodo = (id: string) => {
  return HttpWrapper.delete({
    url: `todos/${id}`,
  });
};
