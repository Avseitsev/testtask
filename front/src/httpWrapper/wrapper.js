// @flow
/* eslint-disable no-else-return */
import axios from "axios";

export interface HttpOptions {
  url: string,
  headers?: Object,
  data?: Object,
}

interface HttpOptionsInternal extends HttpOptions {
  method: string,
}

class HttpWrapper {
  basePath: string;

  apiRequest(options: HttpOptionsInternal): Promise<Object> {
    const { url } = options;

    const config = {
      ...options,
      url: `${this.basePath}/${url}`
    };

    return new Promise((resolve, reject) => {
      axios.request(config)
        .then((response) => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  init(basePath: string): void {
    this.basePath = basePath;
  }

  get(options: HttpOptions): Promise<Object> {
    return this.apiRequest({ ...options, method: "get" });
  }

  put(options: HttpOptions): Promise<Object> {
    return this.apiRequest({ ...options, method: "put" });
  }

  post(options: HttpOptions): Promise<Object> {
    return this.apiRequest({ ...options, method: "post" });
  }

  delete(options: HttpOptions): Promise<Object> {
    return this.apiRequest({ ...options, method: "delete" });
  }
}

const wrapper = new HttpWrapper();

export default wrapper;
